﻿' Pour plus d'informations sur le modèle d'élément Page vierge, consultez la page https://go.microsoft.com/fwlink/?LinkId=234238

''' <summary>
''' Une page vide peut être utilisée seule ou constituer une page de destination au sein d'un frame.
''' </summary>
Public NotInheritable Class Sorties
    Inherits Page
    Private Sub LoadSort(sender As Object, e As RoutedEventArgs)
        retour.IsChecked = True
    End Sub

    Private Sub validerClick(sender As Object, e As RoutedEventArgs) Handles button.Click
        'If op.Text.Contains("bl") <> 0 Or op.Text.Contains("BL") <> 0 Then
        'MsgBox("le champs opérateur doit ne pas contenir le mot clé <BL>", "ok")
        'Exit Sub
        'End If
        If casse.IsChecked = True Then
            prefixe = "BL_"
            suffixe = "Casse_" & op.Text
            Module1.operation = "Bs"
        End If

        If perime.IsChecked = True Then
            prefixe = "BL_"
            suffixe = "perime_" & op.Text
            Module1.operation = "Bs"
        End If

        If vol.IsChecked = True Then
            prefixe = "BL_"
            suffixe = "Vol_" & op.Text
            Module1.operation = "Bs"
        End If

        If retour.IsChecked = True Then
            prefixe = "BL_"
            suffixe = "Retour_Frs_" & op.Text
            Module1.operation = "Bs"
        End If

        If usage.IsChecked = True Then
            prefixe = "BL_"
            suffixe = "Usage_Mg_" & op.Text
            Module1.operation = "Bs"
        End If
        '------------------------------------------------'
        prefixe = prefixe.Replace(" ", "_")
        '-------------------------------------------------'
        Frame.Navigate(GetType(Pointages))
        'form1.code_article.Focus()
        ' Me.Hide()
    End Sub

    Private Sub opGotFocus(sender As Object, e As RoutedEventArgs) Handles op.GotFocus
        '  op.BackColor = Color.Aqua
        op.Select(0, (op.Text).Length())
        ' op.Focus()
    End Sub

    Private Sub retourne(sender As Object, e As RoutedEventArgs)
        Frame.Navigate(GetType(Operations))
    End Sub

    Private Sub bcKeyUp(sender As Object, e As KeyRoutedEventArgs) Handles op.KeyUp
        '  If e.KeyChar = Convert.ToChar(13) Then

        If String.IsNullOrEmpty(op.Text) Then
                Exit Sub
            Else
                op.SelectAll()
            validerClick(Nothing, Nothing)
        End If
        'End If
    End Sub


    Private Sub aide(sender As Object, e As RoutedEventArgs)

    End Sub

    Private Sub fermer(sender As Object, e As RoutedEventArgs)
        Application.Current.Exit()
    End Sub
End Class
