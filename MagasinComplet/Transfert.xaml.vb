﻿Imports System.Data.SqlClient
' Pour plus d'informations sur le modèle d'élément Page vierge, consultez la page https://go.microsoft.com/fwlink/?LinkId=234238

''' <summary>
''' Une page vide peut être utilisée seule ou constituer une page de destination au sein d'un frame.
''' </summary>
Public NotInheritable Class Transfert
    Inherits Page

    Private Function apreviation(ByVal chaine As String) As String
        apreviation = ""
        chaine = chaine.Replace("_", " ")
        chaine = chaine.Replace("-", " ")
        chaine = chaine.Replace("#", " ")
        chaine = chaine.Replace("/", " ")
        chaine = chaine.Replace("\", " ")
        chaine = chaine.Replace("*", " ")

        Dim result() As String = chaine.Split(" ")
        For i = 0 To (result).Length
            apreviation = apreviation & result(i).Substring(1, 1)
        Next
        If apreviation = "" Then apreviation = chaine.Substring(1, 5)

    End Function
    Private Sub cahrger_Magasion(ByVal dest As ComboBox)
        Dim req As String = "select nom from Magasin"
        Dim cmd As New SqlCommand(req, Connexion_serveur_mobile)
        Dim dest_reader As SqlDataReader
        Try
            dest_reader = cmd.ExecuteReader()
            dest.Items.Clear()
            Do While dest_reader.Read
                If Not String.IsNullOrEmpty(dest_reader.GetString(0).ToString) Then
                    dest.Items.Add(dest_reader.GetString(0).ToString)
                End If
            Loop

            If dest.Items.Count > 0 Then
                ' dest.ItemsSource = dest.Items.Item(0).ToString
                ' dest.Text
                dest.Items.Add(dest.Items.Item(0).ToString)
                ' dest.Focus()
                '/ dest.SelectAll()
            End If
            dest_reader.Close()
            cmd.Dispose()
        Catch ex As Exception
            MsgBox("prob chargement liste magazin --> " & ex.Message)
        End Try

    End Sub

    Private Sub loadTransfert(sender As Object, e As RoutedEventArgs)
        connexion_begin()
        If Connexion_serveur_mobile.State = Data.ConnectionState.Open Then
            cahrger_Magasion(Mg2)
            cahrger_Magasion(Mg1)
            connexion_end()
        Else
            Mg1.Items.Add("Mg_Source")
            Mg1.DataContext = Mg1.Items.Item(0).ToString 'Mg1.Text = Mg1.Items.Item(0).ToString
            Mg2.Items.Add("Mg_Dest")
            Mg2.DataContext = Mg2.Items.Item(0).ToString 'Mg2.Text = Mg2.Items.Item(0).ToString
        End If

        ' op.Focus()
    End Sub
    Private Sub actualiser(sender As Object, e As RoutedEventArgs)
        connexion_begin()
        If Connexion_serveur_mobile.State = Data.ConnectionState.Open Then
            cahrger_Magasion(Mg1)
            cahrger_Magasion(Mg2)
            connexion_end()
        Else
            Mg1.Items.Add("Mg_Source")
            Mg1.ItemsSource = Mg1.Items.Item(0).ToString
            Mg2.Items.Add("Mg_Dest")
            Mg2.ItemsSource = Mg2.Items.Item(0).ToString
        End If
        '    op.Focus()
    End Sub

    Private Sub Button_Click_1(sender As Object, e As RoutedEventArgs)

    End Sub

    Private Sub retour(sender As Object, e As RoutedEventArgs)

    End Sub

    Private Sub Button_Click_4(sender As Object, e As RoutedEventArgs)

    End Sub

    Private Sub Button_Click(sender As Object, e As RoutedEventArgs) Handles button.Click
        '  If InStr(op.Text, "tr") <> 0 Or InStr(op.Text, "TR") <> 0 Then
        'MsgBox("le champs opérateur doit ne pas contenir le mot clé <TR>", MsgBoxStyle.Critical)
        '     Exit Sub
        '  End If
        Dim apreviation_mg1 As String = ""
        Dim apreviation_mg2 As String = ""

        If Mg1.SelectedValue = "t" Then 'Mg2.SelectedValue Then
            MsgBox(Mg1.SelectedValue, "ok")
            MsgBox("Erreur :Mg_source = Mg_Destination !!!!")
            Exit Sub
        End If

        'Dim a As String = Mg1.SelectedValue
        'MsgBox(a, " ok")


        '--------------------------------------------------'
        'operateur = op.Text
        prefixe = "TR_"
        operation = "TR"

        apreviation_mg1 = apreviation((Mg1.SelectedValue).Trim)
        apreviation_mg2 = apreviation((Mg2.SelectedValue).Trim)

        '   If operateur <> "" Then
        '  suffixe = operateur & "_" & apreviation_mg1 & "_" & apreviation_mg2
        ' Else
        suffixe = apreviation_mg1 & "_" & apreviation_mg2
        'End If

        '------------------------------------------------'
        prefixe = prefixe.Replace(prefixe, " ", "_")
        '------------------------------------------------'
        Frame.Navigate(GetType(Pointages))

        'Form1.code_article.Focus()
        '   Me.Hide()
    End Sub
End Class
