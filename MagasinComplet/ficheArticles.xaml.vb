﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Data.SqlServerCe
'Imports System.Drawing
Imports MagasinComplet.ficheArticles
' Pour plus d'informations sur le modèle d'élément Page vierge, consultez la page https://go.microsoft.com/fwlink/?LinkId=234238

''' <summary>
''' Une page vide peut être utilisée seule ou constituer une page de destination au sein d'un frame.
''' </summary>
Public NotInheritable Class ficheArticles
    Inherits Page

    Public Function get_code(ByVal buffer As String) As String
        Dim start As Integer
        Dim endd As Integer
        On Error GoTo fin

        start = 1
        endd = buffer.Contains(start, "/") - 1
        get_code = buffer.Substring(1, endd).Trim

        Exit Function
fin:    MsgBox("get_code : la structure du fichier NE est erronée")
    End Function

    Public Function get_qte(ByVal buffer As String) As String
        Dim start As Integer
        Dim endd As Integer
        Dim longeur As Integer
        On Error GoTo fin

        start = buffer.Contains(1, "/") + 1
        endd = (buffer).Length + 1
        longeur = endd - start

        get_qte = buffer.Substring(start, longeur).Trim

        Exit Function
fin:    MsgBox("get_qte : la structure du fichier NE est erronée")
    End Function

    Private Sub ButtonFichNonEnv(sender As Object, e As RoutedEventArgs)
        Dim short_file As String
        dirs = Directory.GetFiles(path_application & "\BONS\", "*NNEEVV.txt")

        'Listfiles.ForeColor = Color.Red
        Listfiles.Items.Clear()
        For Each fichier In dirs
            short_file = fichier.Substring((path_application).Length + 7)
            Listfiles.Items.Add(short_file)
        Next
        '        Listfiles.Refresh()
    End Sub
    Private Sub load_me()
        Dim fichier As String
        Dim short_file As String

        'Listfiles.ForeColor = Color.Blue
        dirs = Directory.GetFiles(path_application & "\BONS\", "*.txt")

        Listfiles.Items.Clear()

        For Each fichier In dirs
            short_file = fichier.Substring((path_application).Length + 7)
            Listfiles.Items.Add(short_file)

        Next fichier

    End Sub
    Private Sub toutFiche(sender As Object, e As RoutedEventArgs)
        Call load_me()
    End Sub

    Private Sub Envoyer(sender As Object, e As RoutedEventArgs)
        Dim fichier As String
        Dim fich As String = ""
        Dim buffer As String
        Dim code As String
        Dim qte As String
        Dim libel As String = ""
        Dim indice As Integer = 1
        Dim terminer As Boolean


        Try
            connexion_begin()
        Catch ex As Exception
            MsgBox("serveur non joignable, vérifiez la connexion wifi", "ok") ' MsgBoxStyle.Critical)
            Exit Sub
        End Try

        fichier = path_application & "\BONS\" '& Listfiles.Text
        If fichier.Contains("NNEEVV") <> 0 Then
            '  fich = fichier.Replace(((InStrRev(fichier, "\")).subString + 1), "_NNEEVV.txt", "")
        Else
            '  fich = fichier.Replace((((InStrRev(fichier, "\") + 1)).Substring), ".txt", "")
        End If

        '------------ verifier si le fichier existe dans la base ----------------'
        If 1 = 2 Then 'file_deja_env(fich) Then
            MsgBox("le fichier selectionné existe déjà dans le seveur", "ok") ' MsgBoxStyle.Critical)
            If fichier.Contains("NNEEVV") <> 0 Then File.Move(fichier, path_application & "\BONS\" & fich & ".txt") : load_me()
            Exit Sub
        End If

        '-------------------------------------------------------------------------'
        Dim cmd_begin_tr As New SqlCommand("begin transaction;", Connexion_serveur_mobile)
        Dim cmd_commit_tr As New SqlCommand("commit transaction;", Connexion_serveur_mobile)
        Dim cmd_roll_tr As New SqlCommand("rollback transaction;", Connexion_serveur_mobile)
        '----------------------------------------ouvrire la connexion vers Article Sdf file ---------------------------------------------------------'
        Connexion_Article_locale = New SqlCeConnection("Data Source=" + path_application + "\\Articles.sdf")
        Try
            If Connexion_Article_locale.State <> ConnectionState.Open Then Connexion_Article_locale.Open()
        Catch ex As Exception
            MsgBox("prob connexion Articles sdf  file --> " & ex.Message)
        End Try
        '--------------------------------------------------'

        indice = 1
        terminer = True
        '------------------------ lire fichier be_mobile.ini -------------------'
        Try
            '---------- debut transaction ------------------'
            Try
                cmd_begin_tr.ExecuteNonQuery()
            Catch ex As Exception
                MsgBox(ex.Message)
            End Try
            '--------------------------------------------------'
            Dim objReader As New System.IO.StreamReader(New MemoryStream(fichier))
            Do While objReader.Peek() <> -1
                buffer = objReader.ReadLine()
                If String.IsNullOrEmpty(buffer) Then Continue Do
                code = get_code(buffer)
                If code.Length > 50 Or code.Contains("/") <> 0 Then MsgBox("Code bizare dans la ligne: " & indice)
                qte = get_qte(buffer)
                ' libel = cherche_article2(code)

                Dim req_insert_server_mobile As String = "insert into " & Table_lignes_pointage & " (nom_file, indice, code_article, lib_article, qte_article) values ('" & fich & "'," & indice & ",'" & code & "','" & libel & "'," & val2(CDbl(qte)) & ")"
                Dim cmd_serveur_mobile As New SqlCommand(req_insert_server_mobile, Connexion_serveur_mobile)
                Try
                    cmd_serveur_mobile.ExecuteNonQuery()
                    'terminer = True
                Catch ex As Exception
                    terminer = False
                    ' MsgBox("prob insertion dans serveur --> " & ex.Message)
                    MsgBox("prob dans la ligne:" & indice & " du fichier : " & fich)
                    Exit Do 'jdida le 24/04/2016
                End Try
                indice = indice + 1
            Loop

            ' objReader.Close()
            If terminer = True Then
                '---------- commit transaction ------------------'
                Try
                    cmd_commit_tr.ExecuteNonQuery()
                Catch ex3 As Exception
                    MsgBox(ex3.Message)
                End Try

                '--------------------renommage et supression de NNEEVV si elle existe------------------'
                Try
                    If fichier.Contains("NNEEVV") <> 0 Then File.Move(fichier, path_application & "\BONS\" & fich & ".txt")
                Catch ex9 As Exception
                    MsgBox("Prob suppression de NNEEVV du fichier local, le nom existe déjà " & ex9.Message)
                End Try
            Else
                '---------- rollback transaction ------------------'
                Try
                    cmd_roll_tr.ExecuteNonQuery()
                Catch ex2 As Exception
                    MsgBox(ex2.Message)
                End Try
                '--------------------------------------------------'
            End If

        Catch ex As Exception
            '---------- rollback transaction ------------------'
            Try
                cmd_roll_tr.ExecuteNonQuery()
            Catch ex2 As Exception
                MsgBox(ex2.Message)
            End Try
            '--------------------------------------------------'
            MsgBox("Err lecture fichier NE " & ex.Message)
        End Try


        connexion_end()
        Connexion_Article_locale.Close()

        If terminer = False Then
            MsgBox("Opération Non terminée ou Serveur non détecté", "ok") ' MsgBoxStyle.Critical)
            Exit Sub
        End If

        MsgBox("Fiches de pointages envoyées avec succès", "ok") ' MsgBoxStyle.Information)
        load_me()
    End Sub


    Private Sub supprimer(sender As Object, e As RoutedEventArgs)
        Dim rep As Integer
        Dim file_supp As String = ""

        '        rep = MsgBox("supprimer le fichier sélectionné ??? ", MsgBoxStyle.YesNo, "Attention")
        If rep = 7 Then
            Exit Sub
        End If

        '   file_supp = path_application & "\BONS\" & Listfiles.Text
        Try
            File.Delete(file_supp)
            '          MsgBox("fichier supprimé", "ok") ' MsgBoxStyle.Information)
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

        Call load_me()
    End Sub

    Private Sub netoyer(sender As Object, e As RoutedEventArgs)
        Dim rep As Integer
        '     rep = MsgBox("supprimer tous les fichiers de pointage ??? ", MsgBoxStyle.YesNo, "Attention")
        If rep = 7 Then
            Exit Sub
        End If

        dirs = Directory.GetFiles(path_application & "\BONS\", "*.txt")

        Try
            For Each fichier In dirs
                File.Delete(fichier)
            Next fichier
            MsgBox("fichiers supprimés", "ok") ' MsgBoxStyle.Information)
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

        Call load_me()
    End Sub

    Private Sub fermer(sender As Object, e As RoutedEventArgs)
        Application.Current.Exit()
    End Sub
End Class
