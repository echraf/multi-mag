﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Data.SqlServerCe
' Pour plus d'informations sur le modèle d'élément Page vierge, consultez la page https://go.microsoft.com/fwlink/?LinkId=234238

''' <summary>
''' Une page vide peut être utilisée seule ou constituer une page de destination au sein d'un frame.
''' </summary>
Public NotInheritable Class Pointages
    Inherits Page


    Dim indice As Integer = 0
    Dim validé As Boolean = False
    Dim nbr_annuler As Integer = 0

    Dim ds As DataSet = New DataSet()
    '------------------------------------- ouvrire fichier txt de saisie---------------------------'
    'Dim nom_file As String = prefixe & Date.Now.ToString("dd-MM-yyyy") & "_" & TimeOfDay.ToString("hh-mm-ss") & "_" & suffixe
    Dim nom_file As String = prefixe & Date.Now.ToString("yyyy-MM-dd") & "_" & DateTime.Now.ToString("hh-mm-ss") & "_" & suffixe
    Dim path_file As String = path_application & "\Bons\" & nom_file & ".txt"
    Dim indice_datagrid As Integer
    Dim code_modif As String
    Dim qte_modif As Double

    Private Sub clear_pointage(ByVal sender As Boolean)
        Dim rep As Integer
        '---------- tester si le bouton été cliquer ou pas -------------------------------------'
        Try
            If sender Then
                '       rep = MsgBox("êtes vous sûr de vouloir supprimer le pointage en cours ", MsgBoxStyle.YesNo, "Attention")
                If rep = 7 Then
                    Exit Sub
                End If
            End If
        Catch ex As Exception
            '  rep = MsgBox("êtes vous sûr de vouloir supprimer le pointage en cours ", MsgBoxStyle.YesNo, "Attention")
            If rep = 7 Then
                Exit Sub
            End If
        End Try
        '----------------------------- supprimer la base sdf locale ------------------------------'
        Dim req As String = "delete from lignes_be_mobile"
        Dim cmd As New SqlCeCommand(req, connectionUser)

        Try
            cmd.ExecuteNonQuery()
        Catch ex As Exception
            MsgBox("prob delete from sdf file --> " & ex.Message)
        End Try
        cmd.Dispose()

        '----------------------- refrech datagrid --------------------------'
        Dim cmd2 As SqlCeCommand = New SqlCeCommand("SELECT indice, code_article, lib_article, qte_article FROM lignes_be_mobile order by indice", connectionUser)
        Dim da As New SqlCeDataAdapter(cmd2)
        ds.Clear()
        da.Fill(ds)
        '  DataGrid1.Refresh()
        cmd2.Dispose()
        '-----------------------------------------------------------------------------------------'
        ordre_modif.Content = nb_records_sdf()
        code_article.Text = ""
        qte_article.Text = ""
        lib_article.Text = ""
        indice = 0
        '  code_article.Focus()
    End Sub

    Function indice_plantage() As Integer
        Dim req As String = "select indice from lignes_be_mobile where code_article = '****' "
        Dim cmd As New SqlCeCommand(req, connectionUser)
        Dim nbr_reader As SqlCeDataReader
        indice_plantage = 0
        Try
            nbr_reader = cmd.ExecuteReader()

            If nbr_reader.Read Then indice_plantage = nbr_reader.GetSqlInt32(0)
            nbr_reader.Close()
            cmd.Dispose()
        Catch ex As Exception
            MsgBox("prob de recherche d'indice plantage --> " & ex.Message)
        End Try

    End Function

    Function nb_records_article_serveur() As Integer
        Dim req As String = "select count(CODART) from " & Nom_table & ",option_prix where codart = cd_art and ch5<>'N'"
        Dim cmd As New SqlCommand(req, Connexion_serveur_mobile)
        Dim nbr_reader As SqlDataReader
        nb_records_article_serveur = 0

        Try
            nbr_reader = cmd.ExecuteReader()

            If nbr_reader.Read Then nb_records_article_serveur = nbr_reader.GetSqlInt32(0)

            nbr_reader.Close()
            cmd.Dispose()
            req = Nothing

        Catch ex As Exception
            MsgBox("prob de comptage des lignes du la table article --> " & ex.Message)
        End Try

    End Function
    Private Sub affiche_frequence_pointage(ByVal code As String)
        Dim req As String = "select  indice,qte_article,code_sup from lignes_be_mobile Where code_article = '" & code & "'"
        Dim cmd_serveur As New SqlCeCommand(req, connectionUser)
        Dim rdr_serveur As SqlCeDataReader
        Dim message As String = "Lignes [" & code & "]" & "\n" ' vbNewLine
        Dim somme As String = 0
        '-------------------------------------- lecture lib article ----------------------------------------'
        Try
            rdr_serveur = cmd_serveur.ExecuteReader()

            Do While rdr_serveur.Read()
                If CInt(rdr_serveur.GetInt32(0)) <> 0 Then
                    If Not String.IsNullOrEmpty(rdr_serveur.GetSqlString(2)) Then
                        message = message & CInt(rdr_serveur.GetInt32(0)) & ": --> " & rdr_serveur.GetSqlDouble(1).ToString & " (Code+)" & "\n" 'vbNewLine
                    Else
                        message = message & CInt(rdr_serveur.GetInt32(0)) & ": --> " & rdr_serveur.GetSqlDouble(1).ToString & "\n" 'vbNewLine
                    End If
                    somme = somme + CInt(val2(rdr_serveur.GetSqlDouble(1)))

                End If
            Loop

            message = message & "Total pointé: " & somme
            MsgBox(message)
            rdr_serveur.Close()
            rdr_serveur.Dispose()
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
        '---------------------------------------------------------------------------------------------------'



    End Sub
    Private Sub Load(sender As Object, e As RoutedEventArgs)
        Call init_application()
    End Sub
    Private Function verifier_etat_article(ByVal code_article) As String
        Dim etat_article As String = "0"
        Dim req As String = "select etat from articles where CODART = '" & code_article & "'"
        Dim cmd As New SqlCeCommand(req, Connexion_Article_locale) 'badelet
        Dim test_article_reader As SqlCeDataReader

        Try
            test_article_reader = cmd.ExecuteReader()

            If test_article_reader.Read Then

                If Not String.IsNullOrEmpty(test_article_reader.GetString(0).ToString) Then etat_article = test_article_reader.GetString(0).ToString

            Else '---- c'est un code supp -------------'

                Dim req2 As String = "select etat from articles where CODART = '" & code_origine & "'"
                Dim cmd2 As New SqlCeCommand(req2, Connexion_Article_locale) 'padel
                Dim test_article_reader2 As SqlCeDataReader

                test_article_reader2 = cmd2.ExecuteReader()

                If test_article_reader2.Read Then
                    If Not String.IsNullOrEmpty(test_article_reader2.GetString(0).ToString) Then etat_article = test_article_reader2.GetString(0).ToString
                End If

                cmd2.Dispose()
            End If


            '/test_article_reader.Close()
            cmd.Dispose()


            Return etat_article

        Catch ex As Exception
            MsgBox("prob verif etat article --> " & ex.Message)
        End Try
        Return ""
    End Function

    Private Function get_qte_par_lot(ByVal code_article) As Double


        Dim req As String = "select qt_lot from Articles where CODART = '" & code_article & "'"
        Dim cmd As New SqlCeCommand(req, Connexion_Article_locale)
        Dim test_article_reader As SqlCeDataReader
        get_qte_par_lot = 1

        Try
            test_article_reader = cmd.ExecuteReader()

            If test_article_reader.Read Then
                If CInt(CDbl(test_article_reader.GetValue(0))) <> 0 Then get_qte_par_lot = CDbl(test_article_reader.GetValue(0))
            Else '---- c'est un code supp -------------'

                Dim req2 As String = "select qt_lot from Articles where CODART = '" & code_origine & "'"
                Dim cmd2 As New SqlCeCommand(req2, Connexion_Article_locale)
                Dim test_article_reader2 As SqlCeDataReader

                test_article_reader2 = cmd2.ExecuteReader()

                If test_article_reader2.Read Then
                    If CInt(CDbl(test_article_reader2.GetValue(0))) <> 0 Then get_qte_par_lot = CDbl(test_article_reader2.GetValue(0))
                End If

                cmd2.Dispose()
            End If

            '/ test_article_reader.Close()
            cmd.Dispose()

        Catch ex As Exception
            MsgBox("prob select qt1 par lot --> " & ex.Message)
        End Try
    End Function

    Public Function cherche_code_origine(ByVal code As String) As String

        Dim req As String = "select  CODE from code_sup Where code_2 = '" & code & "'"
        Dim cmd_serveur As New SqlCeCommand(req, Connexion_Article_locale)
        Dim rdr_serveur As SqlCeDataReader
        cherche_code_origine = code
        '-------------------------------------- lecture lib article ----------------------------------------'
        Try
            rdr_serveur = cmd_serveur.ExecuteReader()

            If rdr_serveur.Read() Then
                If Not String.IsNullOrEmpty(rdr_serveur.GetSqlString(0)) Then
                    cherche_code_origine = rdr_serveur.GetSqlString(0)
                End If

            End If
            '/rdr_serveur.Close()
            rdr_serveur.Dispose()
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
        '---------------------------------------------------------------------------------------------------'




    End Function

    Function cherche_codesup(ByVal indice As Integer) As String
        Dim req As String = "select code_sup from lignes_be_mobile where indice =" & indice
        Dim cmd As New SqlCeCommand(req, connectionUser)
        Dim nbr_reader As SqlCeDataReader
        cherche_codesup = ""

        Try
            nbr_reader = cmd.ExecuteReader()

            If nbr_reader.Read Then cherche_codesup = nbr_reader.GetSqlString(0)
            '/nbr_reader.Close()
            cmd.Dispose()
        Catch ex As Exception
            MsgBox("prob de recherche code_supp dans table bidon --> " & ex.Message)
        End Try

    End Function

    Function nb_records_codesup() As Integer
        Dim req As String = "select count(CODE) from " & nom_table_sup & ",option_prix where CODE = cd_art and ch5<>'N'"
        Dim cmd As New SqlCommand(req, Connexion_serveur_mobile)
        Dim nbr_reader As SqlDataReader
        nb_records_codesup = 0

        Try
            nbr_reader = cmd.ExecuteReader()

            If nbr_reader.Read Then nb_records_codesup = nbr_reader.GetSqlInt32(0)

            '/nbr_reader.Close()
            cmd.Dispose()
            req = Nothing

        Catch ex As Exception
            MsgBox("prob de comptage des lignes du la table code_supp --> " & ex.Message)
        End Try

    End Function

    Function etoiles_existes() As Boolean
        Dim req As String = "select * from lignes_be_mobile where code_article =  '****' "
        Dim cmd As New SqlCeCommand(req, connectionUser)
        Dim etoiles_reader As SqlCeDataReader
        etoiles_existes = False

        Try
            etoiles_reader = cmd.ExecuteReader()
            If etoiles_reader.Read Then etoiles_existes = True
            '/etoiles_reader.Close()
            cmd.Dispose()
        Catch ex As Exception
            MsgBox("prob seection nombre des ****  dans sdf local file --> " & ex.Message)
        End Try


    End Function

    Protected Overrides Sub Finalize()
        Try

            If Connexion_serveur_mobile.State <> Data.ConnectionState.Closed Then Connexion_serveur_mobile.Close()
            Connexion_serveur_mobile.Dispose()
            If Connexion_Article_locale.State <> Data.ConnectionState.Closed Then Connexion_Article_locale.Close()
            Connexion_Article_locale.Dispose()


            If connectionUser.State <> Data.ConnectionState.Closed Then connectionUser.Close()
            connectionUser.Dispose()

        Catch ex As Exception
            MsgBox("prob fermeture application --> " & ex.Message)
        End Try
    End Sub
    Public Sub init_application()

        validé = False
        indice = 0
        '----------------------------------------ouvrire la connexion vers Article Sdf file ---------------------------------------------------------'

        Connexion_Article_locale = New SqlCeConnection("Data Source=" + path_application + "\\Articles.sdf")
        Try
            If Connexion_Article_locale.State <> ConnectionState.Open Then Connexion_Article_locale.Open()
        Catch ex As Exception
            MsgBox("prob connexion Articles sdf  file --> " & ex.Message)
        End Try

        '--------------------------------connexion vers sdf local----------------------------------------------------'
        If (code_article.Text).Trim() <> "init" Then
            Try
                '/      connectionUser = New SqlConnection("Data Source=" + System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase) + "\\base_bidon.sdf")

                If connectionUser.State <> ConnectionState.Open Then connectionUser.Open()
            Catch ex As Exception
                MsgBox("prob connexion sdf local file --> " & ex.Message)
            End Try
        End If

        '-------------------------- creation de la dataset  pour les mouvements ----------------------'
        Dim sql As String = "SELECT id_ligne, code_article, lib_article, qtt_article FROM lignes_be_mobile order by id_ligne"
        Dim cmd2 As SqlCeCommand = New SqlCeCommand(sql, connectionUser)
        Dim da As New SqlCeDataAdapter(cmd2)
        'ds.Clear()
        '/  da.Fill(ds)

        '  DataGrid1.DataContext = ds.Tables(0) 'DataGrid1.DataSource = ds.Tables(0)

        '-----si jamais la table est pleine alors le programme c'est planté et puis lancé de nouveau danc il faut mettra à jour le champs nom_file ---------------------------'
        Dim req As String = "update  lignes_be_mobile set nom_file = '" & nom_file & "'"
        Dim cmd As New SqlCeCommand(req, connectionUser)
        Try
            cmd.ExecuteNonQuery()
        Catch ex As Exception
            MsgBox("prob de update de nom file dans sdf local file --> " & ex.Message)
        End Try

        cmd.Dispose()

        '---------------------------creation style pour datagrid --------------------------------------------------'

        '    Dim tableStyle As New DataGridTableStyle
        '  tableStyle.MappingName = ds.Tables(0).TableName
        '      DataGrid1.TableStyles.Add(tableStyle)
        ' tableStyle.GridColumnStyles("indice").HeaderText = "N°"
        'tableStyle.GridColumnStyles("indice").Width = 22
        'tableStyle.GridColumnStyles("code_article").HeaderText = "Code Article"
        'tableStyle.GridColumnStyles("code_article").Width = 40 '85
        'tableStyle.GridColumnStyles("lib_article").HeaderText = "Libellé Article"
        'tableStyle.GridColumnStyles("lib_article").Width = 130  '85
        'tableStyle.GridColumnStyles("qte_article").HeaderText = "Qte"
        'tableStyle.GridColumnStyles("qte_article").Width = 40
        '       DataGrid1.PreferredRowHeight = 23


        '--------------------------finalement on peut commencer-------------------------------------------------'
        '        DataGrid1.Refresh()

        '---tester si l'application à planté pendant la modification d'un article ----'

        If etoiles_existes() Then
            ordre_modif.Content = indice_plantage()
            If CInt(ordre_modif.Content) > nb_records_sdf() Then
                'DataGrid1.CurrentRowIndex = CInt(ordre_modif.Content) - 2
                ' DataGrid1.Select(CInt(ordre_modif.Content) - 2)
                indice_datagrid = CInt(ordre_modif.Content) - 1
            Else
                '  DataGrid1.CurrentRowIndex = CInt(ordre_modif.Content) - 1
                '   DataGrid1.Select(CInt(ordre_modif.Content) - 1)
                indice_datagrid = CInt(ordre_modif.Content)
            End If

            '  code_article.Text = DataGrid1(DataGrid1.CurrentRowIndex, 1)
            ' lib_article.Text = DataGrid1(DataGrid1.CurrentRowIndex, 2)
            'qte_article.Text = DataGrid1(DataGrid1.CurrentRowIndex, 3)
            '  code_article.Focus()
            code_article.SelectAll()

        Else
            ordre_modif.Content = nb_records_sdf()
            code_article.Text = ""
            'code_article.Focus()
        End If
        '-------------------------------------------------------'
        code_origine_precedent = ""
        ' valide.BackColor = Color.Green
    End Sub

    Private Sub envoyer(sender As Object, e As RoutedEventArgs)
        connexion_begin()

        If nb_records_sdf() = 0 Then
            MsgBox("impossible: la table de mouvments est vide", "ok") ' MsgBoxStyle.Critical)
            ' code_article.Focus()
            Exit Sub
        End If

        If etoiles_existes() Then
            MsgBox("il y a déjà une ligne en cours de modification!!!", "ok") ' MsgBoxStyle.Critical)
            Exit Sub
        End If

        '------------------------------------------------------------------------------------------'
        Dim req_read_sdf As String = "SELECT nom_file, indice, code_article, lib_article, qte_article FROM lignes_be_mobile order by indice"
        Dim cmd_read_sdf As New SqlCeCommand(req_read_sdf, connectionUser)

        Dim cmd_begin_tr As New SqlCommand("begin transaction;", Connexion_serveur_mobile)
        Dim cmd_commit_tr As New SqlCommand("commit transaction;", Connexion_serveur_mobile)
        Dim cmd_roll_tr As New SqlCommand("rollback transaction;", Connexion_serveur_mobile)

        Dim sdf_data_reader As SqlCeDataReader
        Dim etape1 As Boolean = True
        Dim etape2 As Boolean
        '/Dim saisie_file_obj As New System.IO.StreamWriter(path_file)
        '---------- debut transaction ------------------'
        Try
            cmd_begin_tr.ExecuteNonQuery()
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
        '--------------------------------------------------'

        sdf_data_reader = cmd_read_sdf.ExecuteReader()

        Do While sdf_data_reader.Read()

            Dim req_insert_server_mobile As String = "insert into " & Table_lignes_pointage & " (nom_file, indice, code_article, lib_article, qte_article) values ('" & sdf_data_reader.GetString(0).ToString & "'," & CInt(sdf_data_reader.GetInt32(1)) & ",'" & sdf_data_reader.GetString(2).ToString & "','" & sdf_data_reader.GetString(3).ToString & "'," & val2(sdf_data_reader.GetSqlDouble(4).ToString) & ")"
            Dim cmd_serveur_mobile As New SqlCommand(req_insert_server_mobile, Connexion_serveur_mobile)

            '---------Ajouter lignes dans sql server mobile ----------------------'
            Try
                If etape1 = True Then cmd_serveur_mobile.ExecuteNonQuery()
            Catch ex As Exception
                etape1 = False
                MsgBox("Le poinatge sera enregister localement pour une future importation")
            End Try
            '------------Ajouter lignes dans file txt local ----------------------'
            Try
                '/      saisie_file_obj.Write(sdf_data_reader.GetString(2).ToString & "/" & val2(sdf_data_reader.GetSqlDouble(4).ToString) & "\n") 'vbNewLine)
                etape2 = True
            Catch ex As Exception
                MsgBox("Erreur d'enregistrement dans le fichier txt local " & ex.Message)
                etape2 = False
            End Try

            req_insert_server_mobile = Nothing
            cmd_serveur_mobile.Dispose()
        Loop
        '/  sdf_data_reader.Close()

        '----------------------si opéartion insersion dans serveur est totalement complete then commit else rollback -------------------------------------'
        If etape1 = True Then
            '-- la ligne suivante est Seulmement pour ramzi--'
            If Interfaces = 2 And (operation = "invmag" Or operation = "invdepot") Then maj_etat_zone("pointée", (zone_var).Trim, (rayon_var).Trim, "", "fin")
            '---------- commit transaction ------------------'
            Try
                cmd_commit_tr.ExecuteNonQuery()
            Catch ex As Exception
                MsgBox(ex.Message)
            End Try
            '--------------------------------------------------'
        Else
            '---------- rollback transaction ------------------'
            Try
                cmd_roll_tr.ExecuteNonQuery()
            Catch ex As Exception
                MsgBox(ex.Message)
            End Try
            '--------------------------------------------------'
        End If
        '-------------- renommage file local ---------------------'
        If etape2 Then
            Dim path2 As String
            path2 = path_file.Substring(1, (path_file).Length - 4) & "_NNEEVV.txt"
            validé = True
            '/    saisie_file_obj.Close()
            Try
                If etape1 = False Then File.Move(path_file, path2)
                MsgBox("Oppération terminée avec succé", "ok") ' MsgBoxStyle.Information)
            Catch ex As Exception
                MsgBox(ex.Message)
                connexion_end()
                Exit Sub
            End Try
        End If
        '--------------------------- Nettoyer interface et base locale ----------------------------'
        connexion_end()
        '----------------------------------------close la connexion vers Article Sdf file ---------------------------------------------------------'
        Try
            If Connexion_Article_locale.State = ConnectionState.Open Then Connexion_Article_locale.Close()
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
        '------------------------------------------------------------------------'
        Call clear_pointage(False)
        'Call quitter_Click(Nothing, Nothing)
        Application.Current.Exit()
    End Sub

    Private Sub codeGotFocus(sender As Object, e As RoutedEventArgs)
        'code_article.BackColor = Color.Aqua
        code_article.Select(0, (code_article.Text).Length())
        ' code_article.Focus()
    End Sub

    Private Sub codeKeyUp(sender As Object, e As KeyRoutedEventArgs)
        If 1 = 1 Then 'e.KeyChar = Convert.ToChar(13) Then

            Dim lib_art As String
            Dim codesup As String
            Dim qte As Double = 1


            If code_article.Text = "" Then Exit Sub
            If (code_article.Text).Contains("/") <> 0 Then MsgBox("le Code contient un caractère interdit ""/"" ") : Exit Sub

            code_origine = ""
            code_origine = cherche_code_origine((code_article.Text).Trim())
            If code_origine = code_origine_precedent Then beeper(100)
            '------------------------------------------------------'
            If operation = "CT" And verifier_etat_article(code_article.Text) = "1" Then
                MsgBox("Article déjà controlé", "ok") ' MsgBoxStyle.Information)
                lib_article.Text = ""
                ' code_article.Focus()
                Exit Sub
            End If
            '-------------------------------------------------------'

            quantite_par_code = 1
            code_sup = ""                                'variable globale contenant le code sup de l'article en cours
            'code_origine = code_article.Text             'variable globale contenant le code org de l'article en cours

            lib_art = cherche_article((code_article.Text).Trim())

            If Not String.IsNullOrEmpty(lib_art) Then
                lib_article.Text = lib_art
            Else
                codesup = cherche_article_sup((code_article.Text).Trim(), qte)
                If Not String.IsNullOrEmpty(codesup) Then
                    lib_art = cherche_article(codesup)
                    quantite_par_code = qte
                Else
                    lib_art = "Non enregistré"
                    If sonn = 1 Then beeper(100)
                End If
            End If

            lib_article.Text = lib_art
            code_origine_precedent = code_origine
            'qte_article.Focus()
            ' Application.DoEvents()
            If Not etoiles_existes() Then ordre_modif.Content = nb_records_sdf() + 1

        End If
    End Sub

    Private Sub QTGotFocus(sender As Object, e As RoutedEventArgs)
        '.Background = Color.Aqua

    End Sub

    Private Sub annuler(sender As Object, e As RoutedEventArgs)
        '-----------securité pour le boutton annuler -----------'
        nbr_annuler = nbr_annuler + 1
        If nbr_annuler <= 3 Then Exit Sub
        nbr_annuler = 0
        '--------------------------------------------------------'
        code_origine_precedent = ""
        clear_pointage(True)
    End Sub

    Private Sub qtKeyUp(sender As Object, e As KeyRoutedEventArgs)
        If 1 = 1 Then ' e.KeyChar = Convert.ToChar(13) Then
            Dim qte As String = ""
            Dim qte_par_lot As Double = 1

            If (((qte_article.Text).Trim).Substring(1, 1)).ToUpperInvariant = "E" Then ' vérifier c'est un code par lot '
                qte = ((qte_article.Text).Trim).Substring(2)
                qte_par_lot = get_qte_par_lot(code_article.Text)
            Else
                qte = qte_article.Text
            End If
            '--------------------- test sur la quantité ---------------------------------------'

            If 1 = 2 Then 'Not IsNumeric((qte).Trim) Then
                MsgBox("Quatité non valide")
                qte_article.Text = ""
                '   qte_article.Focus()
                Exit Sub
            Else
                If qte < 0 Then
                    MsgBox("Quatité non valide")
                    qte_article.Text = ""
                    'qte_article.Focus()
                    Exit Sub
                End If

            End If

            qte_article.Text = (qte).Trim
            '/ Application.DoEvents()

            '----------------------------------------------------------------------------------------'

            Dim etoiles As Boolean = etoiles_existes()

            If etoiles Then '---procédure d'éxcéption------'


                If CInt(val2(qte_article.Text)) = 0 Then 'tester la qte pour savoir si c'est une operation de modif ou supp 

                    '----------------------supprimer la ligne en ***** dans sdf ---------------------------------'
                    Dim req3 As String = "delete from  lignes_be_mobile where code_article = '****'"
                    Dim cmd3 As New SqlCeCommand(req3, connectionUser) 'badelet

                    Try
                        cmd3.ExecuteNonQuery()
                    Catch ex As Exception
                        MsgBox("prob delete ligne contenat des ****  dans sdf local file --> " & ex.Message)
                    End Try

                    cmd3.Dispose()
                    '/ Application.DoEvents()
                    '----------------------mise a jour des indices après supp de la ligne en ***** dans sdf ---------------------------------'
                    Dim req4 As String = "update  lignes_be_mobile set indice = indice - 1 where indice > " & indice_datagrid
                    Dim cmd4 As New SqlCeCommand(req4, connectionUser) 'badel

                    Try
                        cmd4.ExecuteNonQuery()
                    Catch ex As Exception
                        MsgBox("prob de decrimenation des indices apres supp des ****  dans sdf local file --> " & ex.Message)
                    End Try

                    cmd4.Dispose()
                    '/   Application.DoEvents()
                Else
                    '----------------------tester si le Mr à changer l'emplacement de l'enregistrement ---------------------------------'
                    indice = nb_records_sdf()
                    If CInt(ordre_modif.Content) <> indice_datagrid Then ' il ya vraiment un changement d'emplacement  '

                        If CInt(ordre_modif.Content) > indice Or CInt(ordre_modif.Content) <= 0 Then 'si le mr introduit un indice > au nombre des mouvements
                            MsgBox("ordre <=0 ou  > nombre de mouvements", "ok") ' MsgBoxStyle.Critical)
                            Exit Sub
                        End If

                        Dim req4 As String
                        If CInt(ordre_modif.Content) < indice_datagrid Then
                            req4 = "update  lignes_be_mobile set indice = indice + 1 where indice between " & CInt(ordre_modif.Content) & " and " & (indice_datagrid - 1)
                        Else
                            req4 = "update  lignes_be_mobile set indice = indice - 1 where indice between " & (indice_datagrid + 1) & " and " & (CInt(ordre_modif.Content))
                        End If

                        Dim cmd4 As New SqlCeCommand(req4, connectionUser)

                        Try
                            cmd4.ExecuteNonQuery()
                        Catch ex As Exception
                            MsgBox("prob de M.A.J des  indices après changement d'ordre  --> " & ex.Message)
                        End Try

                        cmd4.Dispose()
                        '/  Application.DoEvents()
                    End If
                    '----------------------modifier la ligne en ***** dans sdf ---------------------------------'
                    Dim qtes2 As Double
                    qtes2 = CDbl(qte_article.Text) * quantite_par_code * qte_par_lot
                    Dim req3 As String = "update  lignes_be_mobile set indice = '" & CInt(ordre_modif.Content) & "', code_article = '" & code_origine & "', lib_article ='" & lib_article.Text & "', qte_article = " & val2(qtes2.ToString) & " where code_article = '****' ;"
                    Dim cmd3 As New SqlCeCommand(req3, connectionUser)

                    Try
                        cmd3.ExecuteNonQuery()
                    Catch ex As Exception
                        MsgBox("prob update ligne contenat des ****  dans sdf local file --> " & ex.Message)
                    End Try
                    cmd3.Dispose()
                    '/ Application.DoEvents()

                End If

            Else '---procédure d'insertion ou normale------'
                indice = nb_records_sdf()
                If CInt(ordre_modif.Content) <> (nb_records_sdf() + 1) Then  '---- procédure d'insertion ----'

                    If CInt(ordre_modif.Content) > indice Or CInt(ordre_modif.Content) <= 0 Then
                        MsgBox("ordre <= 0 ou  > nombre de mouvements", "ok") ' MsgBoxStyle.Critical)
                        Exit Sub
                    End If

                    '------------------ décallage des indices après ajout ------------------'
                    Dim req4 As String = "update  lignes_be_mobile set indice = indice + 1 where indice >= " & CInt(ordre_modif.Content)
                    Dim cmd4 As New SqlCeCommand(req4, connectionUser)

                    Try
                        cmd4.ExecuteNonQuery()
                    Catch ex As Exception
                        MsgBox("prob d'incrimenation des indices apres décallage des mouvements  dans sdf local file --> " & ex.Message)
                    End Try

                    cmd4.Dispose()
                    '/Application.DoEvents()
                    '----------------- insertion avec l'indice choisi ---------------'
                    Dim qtes1 As Double
                    qtes1 = CDbl(qte_article.Text) * quantite_par_code * qte_par_lot
                    Dim req As String = "insert into lignes_be_mobile (nom_file, indice, code_article, lib_article, qte_article,code_sup) values ('" & nom_file & "'," & CInt(ordre_modif.Content) & ",'" & code_origine & "','" & lib_article.Text & "'," & val2(qtes1.ToString) & ",'" & code_sup & "')"
                    Dim cmd As New SqlCeCommand(req, connectionUser)

                    Try
                        cmd.ExecuteNonQuery()
                    Catch ex As Exception
                        MsgBox("prob insertion spéciale dans sdf file --> " & ex.Message)
                    End Try

                    cmd.Dispose()
                    '/ Application.DoEvents()
                Else 'procédure  normale 100%

                    '---------- vérifier si la quantité est valide -----------------------------------------------'
                    Dim adapt_qte = CInt(val2(qte_article.Text))
                    If adapt_qte <= 0 Then '/ Or Not IsNumber(adapt_qte) Then
                        MsgBox("Quantité non valide")
                        Exit Sub
                    End If
                    '----------------------ajouter nouvelle ligne dans sdf ---------------------------------'
                    Dim qtes As Double
                    indice = nb_records_sdf() + 1
                    qtes = CDbl(qte_article.Text) * quantite_par_code * qte_par_lot
                    Dim req As String = "insert into lignes_be_mobile (nom_file, indice, code_article, lib_article, qte_article,code_sup) values ('" & nom_file & "'," & indice & ",'" & code_origine & "','" & lib_article.Text & "'," & val2(qtes.ToString) & ",'" & code_sup & "')"
                    Dim cmd As New SqlCeCommand(req, connectionUser)

                    Try
                        cmd.ExecuteNonQuery()
                    Catch ex As Exception
                        MsgBox("prob insertion sdf file --> " & ex.Message)
                    End Try

                    cmd.Dispose()
                    '/    Application.DoEvents()
                End If
            End If

            '----------------------- refrech datagrid --------------------------'
            Dim cmd2 As SqlCeCommand = New SqlCeCommand("SELECT indice, code_article, lib_article, qte_article FROM lignes_be_mobile order by indice", connectionUser)
            '/  Dim da As New SqlDataAdapter(cmd2)
            '/ds.Clear()
            '/da.Fill(ds)
            '/   DataGrid1.Refresh()
            cmd2.Dispose()
            '/ Application.DoEvents()+
            '------------------------- init interface --------------------------'
            code_article.Text = ""
            lib_article.Text = ""
            qte_article.Text = ""
            code_origine = ""
            ' code_article.Focus()
            ordre_modif.Content = nb_records_sdf()
            '--------------------------------------------------------------------'*
            If CInt(ordre_modif.Content) = 0 Then Exit Sub
            '/DataGrid1.Select(CInt(ordre_modif.Content) - 1)
            '/DataGrid1.CurrentRowIndex = CInt(ordre_modif.Content) - 1
            '/Application.DoEvents()
        End If
        '/Application.DoEvents()
    End Sub

    Private Sub QTLostFocus(sender As Object, e As RoutedEventArgs)
        'qte_article.Background = Color.White
    End Sub

    Private Sub actualise(sender As Object, e As RoutedEventArgs)

    End Sub

    Private Sub fermer(sender As Object, e As RoutedEventArgs)

    End Sub

    Private Sub nbKeyUp(sender As Object, e As KeyRoutedEventArgs)

    End Sub

    Private Sub codeLostFocus(sender As Object, e As RoutedEventArgs)

    End Sub
End Class
