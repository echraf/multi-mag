﻿Imports System.Data.SqlClient
'Imports System.Data.SqlServerCe
' Pour plus d'informations sur le modèle d'élément Page vierge, consultez la page https://go.microsoft.com/fwlink/?LinkId=234238

''' <summary>
''' Une page vide peut être utilisée seule ou constituer une page de destination au sein d'un frame.
''' </summary>
Public NotInheritable Class Inventaire
    Inherits Page

    Private Sub cahrger_rayons()
        Dim req As String = "select RAY_LIB from rayon"
        Dim cmd As New SqlCommand(req, Connexion_serveur_mobile)
        Dim rayon_reader As SqlDataReader

        Try

            rayon_reader = cmd.ExecuteReader()
            rayon.Items.Clear()

            Do While rayon_reader.Read
                If Not String.IsNullOrEmpty(rayon_reader.GetString(0).ToString) Then
                    rayon.Items.Add(rayon_reader.GetString(0).ToString)
                End If
            Loop

            If rayon.Items.Count > 0 Then
                rayon.ItemsSource = rayon.Items.Item(0).ToString
                ' rayon.Focus()
                '/rayon.SelectAll()
            End If

            '  rayon_reader.Close()
            cmd.Dispose()

        Catch ex As Exception
            MsgBox("prob chargement liste rayons --> " & ex.Message)
        End Try
    End Sub

    Private Sub validerClick(sender As Object, e As RoutedEventArgs) Handles valider.Click

        If 1 = 2 Then '/InStr(op.Text "inv") <> 0 Or InStr(op.Text, "INV") <> 0 Then
            MsgBox("le champs opérateur doit ne pas contenir le mot clé <INV>", "ok") ' MsgBoxStyle.Critical)
            Exit Sub
        End If

        If magazin.IsChecked = True Then
            'If Not String.IsNullOrEmpty(op.Text) Then
            '  prefixe = "IM_" & op.Text & "#" & rayon.ItemsSource & "-" & zone.ItemsSource & "#"

            '        Else
            '           prefixe = "IM#" & rayon.ItemsSource & "-" & zone.ItemsSource & "#"

            '        End If

            If String.IsNullOrEmpty(rayon.ItemsSource) Or String.IsNullOrEmpty(zone.ItemsSource) Then
                MsgBox("Entrer rayon et zone", "ok") ' MsgBoxStyle.Critical)
                Exit Sub
            End If

            operation = "invmag"
        End If

        If depot.IsChecked = True Then
            ' If Not String.IsNullOrEmpty(op.Text) Then
            '  prefixe = "ID_" & op.Text & "#" & rayon.ItemsSource & "-" & zone.ItemsSource & "#"

            '        Else
            '           prefixe = "ID#" & rayon.ItemsSource & "-" & zone.ItemsSource & "#"

            '        End If
            If String.IsNullOrEmpty(rayon.ItemsSource) Or String.IsNullOrEmpty(zone.ItemsSource) Then
                MsgBox("Entrer rayon et zone", "ok") ' MsgBoxStyle.Critical)
                Exit Sub
            End If

            operation = "invdepot"
        End If
        '---------------------------------------------------------------'
        'prefixe = Replace(prefixe, " ", "_")
        '---------------------------------------------------------------'
        If rayon.IsEnabled = False Then GoTo suite
        '---------------------------------------------------------------'
        If 1 = 1 Then 'operation = "invmag" Or operation = "invdepot" Then
            connexion_begin()
            Select Case etat_pointage(oper, ((zone.ItemsSource).Trim()), ((rayon.ItemsSource).Trim()))
                Case "pointée"
                    MsgBox("Zone déjà pointée par " & oper, "Ok") ' MsgBoxStyle.Critical)
                    Exit Sub
                Case "pointage en cours"
                    MsgBox("Zone en cours de pointage par " & oper, "Ok") ' MsgBoxStyle.Critical)
                    Exit Sub
            End Select
            '           If op.Text <> "" Then oper = op.Text
            maj_etat_zone("pointage en cours", ((zone.ItemsSource).Trim()), ((rayon.ItemsSource).Trim()), "deb", "rien")
            connexion_end()
        End If
        '-------------------------------------------------------------------------'
        rayon_var = rayon.ItemsSource
        zone_var = zone.ItemsSource
suite:  Frame.Navigate(GetType(Pointages))

        ' Pointage.code_article.Focus()
        '  Me.Hide()

    End Sub

    Private Sub actualiser(sender As Object, e As RoutedEventArgs)
        connexion_begin()
        If Connexion_serveur_mobile.State = Data.ConnectionState.Open Then
            rayon.IsEnabled = True
            zone.IsEnabled = True
            cahrger_rayons()
            connexion_end()
        Else
            rayon.IsEnabled = False
            zone.IsEnabled = False
            rayon.Items.Add("Rayonxxx")
            rayon.ItemsSource = rayon.Items.Item(0).ToString
            zone.Items.Add("Zonexxx")
            zone.ItemsSource = zone.Items.Item(0).ToString

            'quitter.Enabled = False
        End If
        ' magazin.Focus()

    End Sub

    Private Sub rayonChanged(sender As Object, e As SelectionChangedEventArgs) Handles rayon.SelectionChanged
        If rayon.ItemsSource = "Rayonxxx" Then Exit Sub
        Dim req As String = "select zone,etat from " & nom_table_zone & " where rayon = '" & rayon.ItemsSource & "' and etat <> 'pointée'  and etat <> 'pointage en cours'"
        Dim cmd As New SqlCommand(req, Connexion_serveur_mobile)
        Dim zone_reader As SqlDataReader

        connexion_begin()

        Try
            zone_reader = cmd.ExecuteReader()

            zone.Items.Clear()

            Do While zone_reader.Read
                If Not String.IsNullOrEmpty(zone_reader.GetString(0).ToString) Then
                    zone.Items.Add(zone_reader.GetString(0).ToString)
                End If
            Loop

            If zone.Items.Count > 0 Then
                zone.ItemsSource = zone.Items.Item(0).ToString
                '        zone.Focus()
                ' zone.SelectAll()
            End If

            '  zone_reader.Close()
            cmd.Dispose()

        Catch ex As Exception
            MsgBox("prob chargement liste zones --> " & ex.Message)
        End Try

        connexion_end()

        'rayon.Focus()

    End Sub

    Private Sub zoneChanged(sender As Object, e As SelectionChangedEventArgs) Handles zone.SelectionChanged
        connexion_begin()
        etat_pointage(oper, ((zone.ItemsSource).Trim()), (rayon.ItemsSource).Trim())
        'op.Text = oper
        connexion_end()
    End Sub

    Private Sub fermer(sender As Object, e As RoutedEventArgs)

    End Sub

    Private Sub aide(sender As Object, e As RoutedEventArgs)

    End Sub

    Private Sub retourner(sender As Object, e As RoutedEventArgs)

    End Sub
End Class
