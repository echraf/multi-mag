﻿Imports MagasinComplet.Module1
' Pour plus d'informations sur le modèle d'élément Page vierge, consultez la page https://go.microsoft.com/fwlink/?LinkId=234238

''' <summary>
''' Une page vide peut être utilisée seule ou constituer une page de destination au sein d'un frame.
''' </summary>
Public NotInheritable Class Configs
    Inherits Page
    Dim dirs As String()
    Dim lines(15) As String
    Dim liness(6) As String
    Dim nbr_magg As Integer = 0
    Private Sub clear_list()
        Array.Clear(lines, 0, lines.Length)
    End Sub
    Private Sub ecrire_multi_mag(ByVal line_nbr As Integer, ByVal nom_mg As String)
        Dim FILE_NAME As String = path_application & "\Multi_magasin.txt"
        Dim FILEE_NAME As Stream = New MemoryStream(FILE_NAME) 'StringReader(FILE_NAME)
        Dim objWriter As New System.IO.StreamWriter(FILEE_NAME)
        Dim j As Integer = 0


        '----------- charger le file dans un tableau de chaines ---------'
        liness(line_nbr - 1) = "Mg" & line_nbr & " :#" & (nom_mg).Trim & "#"
        '-------- on enregistre le nouveau fichier
        Do While (Not liness(j) Is Nothing)
            objWriter.Write(liness(j) & "\n") 'vbNewLine)
            j = j + 1
        Loop
        '/   objWriter.Close()
    End Sub
    Private Function magasin_exciste(ByRef mag As String) As Boolean
        magasin_exciste = False
        Try
            Dim chaine As Stream = New MemoryStream(path_application & "\Multi_magasin.txt")
            Using sr As New StreamReader(chaine)
                Dim line As String
                line = sr.ReadToEnd()
                If line.Contains((mag).Trim) <> 0 Then magasin_exciste = True
                '/   sr.Close()
            End Using

        Catch e As Exception
            MsgBox("Multi_magasin.txt est absent !!!", "OK") ' MsgBoxStyle.Critical)
        End Try
    End Function

    Private Sub Actualiser(sender As Object, e As RoutedEventArgs)
        If (mag.Text.Contains("#")) <> 0 Or (mag.Text.Contains("*")) <> 0 Or (mag.Text.Contains("/")) <> 0 Or (mag.Text.Contains("\")) <> 0 Then MsgBox("nom magasin non standard", "Ok") 'MsgBoxStyle.Critical) : Exit Sub
        If ((ip.Text).Trim()) = "" Or ((base.Text).Trim()) = "" Then MsgBox("Champs Vides !!!", "ok") : Exit Sub 'MsgBoxStyle.Exclamation
        If magasin_exciste((mag.Text).Trim()) Then MsgBox("Magasin exicte déjà !!!", "ok") : Exit Sub 'MsgBoxStyle.Exclamation
        If nbr_mag > 5 Then MsgBox("5 magasins MAX !!!", "ok") : Exit Sub 'MsgBoxStyle.Exclamation
        '---------------- Modifier Multi_magasin.txt ------------'

        Dim Path As String = path_application & "\Multi_magasin.txt"
        'Using sw As StreamWriter = File.AppendText(Path)
        '    sw.WriteLine(vbNewLine & "Mg" & nbr_mag + 1 & " :#" & Files.Text & "#")
        'End Using
        ecrire_multi_mag(nbr_mag + 1, mag.Text)
        '--------------- Ajouter fichier magasinX.txt -------------'
        Path = path_application & "\magasin" & nbr_mag + 1 & ".txt"
        Using sw As StreamWriter = File.CreateText(Path)
            sw.WriteLine("chemin_base :#" & ip.Text & "#")
            sw.WriteLine("Nom_catalog :#" & base.Text & "#")
        End Using

        MsgBox("Magasin Ajouté avec succés", "ok") ' MsgBoxStyle.Information)
        '          load_me()
    End Sub


    Private Sub Help(sender As Object, e As RoutedEventArgs)
        MsgBox("Pour l'adresse ip : - Si la pointeuse est DS5 il faut écrire : 192.168.1.x\SERVER \n  - Si la pointeuse est Motorola il faut écrire : 192.168.1.x,1433")
        MsgBox("Pour Aouter un magasin : Remplir les champs puis clicker sur + Dans le champs fichier donner le nom du nouveau magasin", "ok") ' MsgBoxStyle.Information)

    End Sub

    Private Sub Fermer(sender As Object, e As RoutedEventArgs)
        'Me.Hide()

        If Multi_mag = 0 Then
            Select Case Interfaces
             '   Case 1 : start_form.Show()
                Case 2 : Frame.Navigate(GetType(Operations))
                    '  Case 3 : y = 34 : visible_import = False : hamza.Show()
            End Select
        Else
            Frame.Navigate(GetType(Acceuil))
        End If
        Application.Current.Exit()
    End Sub

    Private Sub FichierSelectChanged(sender As Object, e As SelectionChangedEventArgs) Handles Files.SelectionChanged
        On Error GoTo fin
        Dim strLine As String
        Dim FILE_NAME As String = path_application & "\" & Files.Items.ToString()
        Dim objreader As New System.IO.StreamReader(New MemoryStream(FILE_NAME))
        Dim Str As String
        Dim strArr() As String
        Dim i As Integer = 0

        strLine = objreader.ReadLine

        '--------- fichier vide --------'
        If strLine Is Nothing Or strLine.Trim() = "" Then
            '/  objreader.Close()
            objreader.Dispose()
            ip.Text = ""
            base.Text = ""
            multi.IsChecked = False
            multi.IsEnabled = False
            clear_list()
            Exit Sub
        End If

        '----------- charger le file dans un tableau de chaines ---------'
        Do While (Not strLine Is Nothing)
            lines(i) = strLine
            strLine = objreader.ReadLine
            i = i + 1
        Loop

        '---------------------------------------------------------------------'

        If (Files.ItemsSource).ToUpper() = "POINTAGE_MOBILE.TXT" Then   '-- si Config generale --'

            Str = lines(0)
            strArr = Str.Split("#")
            ip.Text = strArr(1)

            Str = lines(2)
            strArr = Str.Split("#")
            base.Text = strArr(1)


            Str = lines(10)
            strArr = Str.Split("#")
            multi.IsEnabled = True
            multi.IsChecked = CInt(strArr(1))

            Str = lines(12)
            strArr = Str.Split("#")
            If Str = "" Then son.IsChecked = 1 : GoTo suiv
            son.IsChecked = CInt(strArr(1))

        Else '------------------------------------------------  Config magasins --'
            Str = lines(0)
            strArr = Str.Split("#")
            ip.Text = strArr(1)

            Str = lines(1)
            strArr = Str.Split("#")
            base.Text = strArr(1)

            multi.IsChecked = False
            multi.IsEnabled = False
        End If

        '--------------- lire multi_mag.text ----------------'
suiv:   If (Files.Items.ToString()).ToUpper = "POINTAGE_MOBILE.TXT" Then mag.Text = "########" : GoTo nexxtt
        Dim num_mag As Integer
        If ((Files.Items.ToString()).Contains("magasin")) <> 0 Then num_mag = CInt((Files.ItemsSource).SubStr(8, 1))
        Str = liness(num_mag - 1)
        strArr = Str.Split("#")
        mag.Text = strArr(1)
        'lire_multi_mag(num_mag, "")

        '-----------------------------------------------------'

nexxtt: 'objreader.Close()
        objreader.Dispose()
        Exit Sub
fin:
        MsgBox("problem") 'Err.Description)
    End Sub


    Private Sub Button_Click_3(sender As Object, e As RoutedEventArgs)
        Frame.Navigate(GetType(ficheArticles))
    End Sub

    Private Sub Button_Click_4(sender As Object, e As RoutedEventArgs)
        'Me.Hide()

        If Multi_mag = 0 Then
            Select Case Interfaces
              '  Case 1 : start_form.Show()
                Case 2 : Frame.Navigate(GetType(Operations))
                    '  Case 3 : y = 34 : visible_import = False : hamza.Show()
            End Select
        Else
            Frame.Navigate(GetType(Acceuil))
        End If
        Application.Current.Exit()
    End Sub

    Private Sub btnConfirm(sender As Object, e As RoutedEventArgs)
        'If son.IsChecked = True Then
        'sonn = 1
        'Else
        'sonn = 0
        'End If
        '------------------ file valide ou pas -------------------'

        If ((Files.SelectedValue).Trim()) = "" Or ((Files.SelectedValue).Trim()) = "Aucun fichier" Then
            MsgBox("fichier invalide")
            Exit Sub
        End If
        '---------------------- Sauvegarde config file -----------------------'
        Dim FILE_NAME As String = "C:\Users\baseInfo\Documents\Visual Studio 2017\Projects\MagasinComplet\MagasinComplet\bin\x86\Debug\AppX\Multi_magasin.txt" ' & "\" & Files.SelectedValue
        MsgBox(Files.SelectedValue, "lalalallalalalal")
        MsgBox(FILE_NAME, "ooooooooooooooo")
        Dim objWriter As StreamWriter = New StreamWriter(FILE_NAME)
        Dim j As Integer = 0
        Try
            '------- Config magasins -----------------------------------------------------------'
            If (Files.Items.ToString()).ToUpper <> "POINTAGE_MOBILE.TXT" Then
                objWriter.Write("chemin_base : #" & ((ip.Text).Trim()) & "#         ")
                objWriter.Write("Nom_catalog : #" & ((base.Text).Trim()) & "#       ") ' vbNewLine)
                objWriter.Close()
                '------------ ecrire dans multi_mag ---------------'
                Dim num_mag As Integer
                If ((Files.Items.ToString()).Contains("magasin")) <> 0 Then num_mag = CInt((Files.ItemsSource).substr(8, 1))
                ecrire_multi_mag(num_mag, mag.Text)
                MsgBox(Files.Items.ToString() & " Sauvegardé avec succès", "ok") ' MsgBoxStyle.Information)
                '--------------------------------------------------'
                Exit Sub
            End If

            '------- Config principale(modification de deux lignes)-----------------------------'
            Dim mmg As Integer
            If multi.IsChecked Then mmg = 1 Else mmg = 0

            lines(0) = "chemin_base : #" & ((ip.Text).Trim()) & "#"
            lines(2) = "Nom_catalog : #" & ((base.Text).Trim()) & "#"
            lines(10) = "Multi_mag: #" & mmg & "#"
            lines(12) = "Son : #" & sonn & "#"
            Multi_mag = mmg
            '-------- on enregistre le nouveau fichier
            Do While (Not lines(j) Is Nothing)
                objWriter.Write(lines(j) & "       \n  ") ' vbNewLine)
                j = j + 1
            Loop
            '/  objWriter.Close()



            MsgBox(Files.Items.ToString() & " Sauvegardé avec succès", "ok") ' MsgBoxStyle.Information)
        Catch ex As Exception
            MsgBox(ex.Message, "ok")
        End Try
    End Sub
    Private Function lire_multi_mag(ByVal line_nbr As Integer, ByVal action As String) As String
        Dim strLine As String
        Dim FILE_NAME As String = path_application & "\Multi_magasin.txt"
        '/ Dim objreader As New System.IO.StreamReader(FILE_NAME)
        Dim Str As String
        Dim strArr() As String
        Dim i As Integer = 0

        Array.Clear(liness, 0, liness.Length)

        lire_multi_mag = ""
        '/strLine = objreader.ReadLine
        '--------- fichier vide --------'
        If strLine Is Nothing Or (strLine).Trim = "" Then
            '/  objreader.Close()
            '/objreader.Dispose()
            Exit Function
        End If
        '----------- charger le file dans un tableau de chaines ---------'
        Do While (Not strLine Is Nothing)
            liness(i) = strLine
            '/strLine = objreader.ReadLine
            i = i + 1
        Loop

        If action = "rien" Then GoTo suite

        Str = liness(line_nbr - 1)
        strArr = Str.Split("#")
        lire_multi_mag = strArr(1)

suite:'/  objreader.Close()
        '/objreader.Dispose()
    End Function
    Private Sub load_me()
        Dim fichier As String
        Dim short_file As String
        confirm.IsEnabled = True
        dirs = Directory.GetFiles("C:\Users\baseInfo\Documents\Visual Studio 2017\Projects\MagasinComplet\MagasinComplet\bin\x86\Debug\AppX" & "\", "*.txt") 'path_application & "\", "*.txt")
        Array.Sort(dirs)
        Files.Items.Clear()
        ip.Text = ""
        base.Text = ""

        If dirs.Length = 0 Then Files.Items.Add("Aucun fichier") : confirm.IsEnabled = False : Exit Sub

        Try
            For Each fichier In dirs
                short_file = fichier.Substring(("C:\Users\baseInfo\Documents\Visual Studio 2017\Projects\MagasinComplet\MagasinComplet\bin\x86\Debug\AppX").Length + 1)
                If short_file.Contains("magasin") <> 0 Then nbr_magg = nbr_magg + 1
                If (short_file).ToUpper <> "MULTI_MAGASIN.TXT" Then Files.Items.Add(short_file)
            Next fichier
            nbr_mag = nbr_magg - 1
            mag.Text = lire_multi_mag(1, "rien")
        Catch ex As Exception
            '   MsgBox(ex.Message)
        End Try
    End Sub
    Private Sub load(sender As Object, e As RoutedEventArgs)
        load_me()
    End Sub
End Class


