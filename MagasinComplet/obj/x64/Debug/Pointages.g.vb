﻿#ExternalChecksum("C:\Users\baseInfo\Documents\Visual Studio 2017\Projects\MagasinComplet\MagasinComplet\Pointages.xaml", "{406ea660-64cf-4c82-b6f0-42d48172a799}", "16C6967EE34836D6693FFE9DDE799023")
'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated by a tool.
'
'     Changes to this file may cause incorrect behavior and will be lost if
'     the code is regenerated.
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict Off
Option Explicit On

Namespace Global.MagasinComplet

    Partial Class Pointages
        Implements Global.Windows.UI.Xaml.Markup.IComponentConnector
        Implements Global.Windows.UI.Xaml.Markup.IComponentConnector2


        <Global.System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.Windows.UI.Xaml.Build.Tasks", " 10.0.16.0")>  _
        <Global.System.Diagnostics.DebuggerNonUserCodeAttribute()>  _
        Public Sub Connect(ByVal connectionId As Integer, ByVal target As Global.System.Object) Implements Global.Windows.UI.Xaml.Markup.IComponentConnector.Connect
            Select Case connectionId
            Case 1 ' Pointages.xaml line 11
                    Dim element1 As Global.Windows.UI.Xaml.Controls.Grid = CType(target, Global.Windows.UI.Xaml.Controls.Grid)
                AddHandler DirectCast(element1, Global.Windows.UI.Xaml.Controls.Grid).Loaded, AddressOf Me.Load
                    Exit Select
            Case 2 ' Pointages.xaml line 12
                    Dim element2 As Global.Windows.UI.Xaml.Controls.Button = CType(target, Global.Windows.UI.Xaml.Controls.Button)
                AddHandler DirectCast(element2, Global.Windows.UI.Xaml.Controls.Button).Click, AddressOf Me.actualise
                    Exit Select
            Case 3 ' Pointages.xaml line 22
                    Dim element3 As Global.Windows.UI.Xaml.Controls.Button = CType(target, Global.Windows.UI.Xaml.Controls.Button)
                AddHandler DirectCast(element3, Global.Windows.UI.Xaml.Controls.Button).Click, AddressOf Me.fermer
                    Exit Select
            Case 4 ' Pointages.xaml line 32
                    Me.DataGrid1 = CType(target, Global.Windows.UI.Xaml.Controls.Grid)
                    Exit Select
            Case 5 ' Pointages.xaml line 35
                    Me.valide = CType(target, Global.Windows.UI.Xaml.Controls.Button)
                AddHandler DirectCast(Me.valide, Global.Windows.UI.Xaml.Controls.Button).Loaded, AddressOf Me.envoyer
                AddHandler DirectCast(Me.valide, Global.Windows.UI.Xaml.Controls.Button).Click, AddressOf Me.envoyer
                    Exit Select
            Case 6 ' Pointages.xaml line 38
                    Me.code_article = CType(target, Global.Windows.UI.Xaml.Controls.TextBox)
                AddHandler DirectCast(Me.code_article, Global.Windows.UI.Xaml.Controls.TextBox).GotFocus, AddressOf Me.codeGotFocus
                AddHandler DirectCast(Me.code_article, Global.Windows.UI.Xaml.Controls.TextBox).KeyUp, AddressOf Me.codeKeyUp
                AddHandler DirectCast(Me.code_article, Global.Windows.UI.Xaml.Controls.TextBox).LostFocus, AddressOf Me.codeLostFocus
                    Exit Select
            Case 7 ' Pointages.xaml line 39
                    Me.ordre_modif = CType(target, Global.Windows.UI.Xaml.Controls.Button)
                AddHandler DirectCast(Me.ordre_modif, Global.Windows.UI.Xaml.Controls.Button).KeyUp, AddressOf Me.nbKeyUp
                    Exit Select
            Case 8 ' Pointages.xaml line 40
                    Me.lib_article = CType(target, Global.Windows.UI.Xaml.Controls.TextBox)
                    Exit Select
            Case 9 ' Pointages.xaml line 42
                    Me.qte_article = CType(target, Global.Windows.UI.Xaml.Controls.TextBox)
                AddHandler DirectCast(Me.qte_article, Global.Windows.UI.Xaml.Controls.TextBox).GotFocus, AddressOf Me.QTGotFocus
                AddHandler DirectCast(Me.qte_article, Global.Windows.UI.Xaml.Controls.TextBox).KeyUp, AddressOf Me.qtKeyUp
                AddHandler DirectCast(Me.qte_article, Global.Windows.UI.Xaml.Controls.TextBox).LostFocus, AddressOf Me.QTLostFocus
                    Exit Select
            Case 10 ' Pointages.xaml line 43
                    Me.Button1 = CType(target, Global.Windows.UI.Xaml.Controls.Button)
                AddHandler DirectCast(Me.Button1, Global.Windows.UI.Xaml.Controls.Button).Click, AddressOf Me.annuler
                    Exit Select
            Case 11 ' Pointages.xaml line 33
                    Me.pg = CType(target, Global.Windows.UI.Xaml.Controls.ProgressBar)
                    Exit Select
                    Case Else
                        Exit Select
            End Select
                Me._contentLoaded = true
        End Sub

        <Global.System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.Windows.UI.Xaml.Build.Tasks", " 10.0.16.0")>  _
        <Global.System.Diagnostics.DebuggerNonUserCodeAttribute()>  _
        Public Function GetBindingConnector(connectionId As Integer, target As Object) As Global.Windows.UI.Xaml.Markup.IComponentConnector Implements Global.Windows.UI.Xaml.Markup.IComponentConnector2.GetBindingConnector
            Dim returnValue As Global.Windows.UI.Xaml.Markup.IComponentConnector = Nothing
            Return returnValue
        End Function
    End Class

End Namespace


