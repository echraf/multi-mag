﻿Imports MagasinComplet.Module1

' Pour plus d'informations sur le modèle d'élément Page vierge, consultez la page https://go.microsoft.com/fwlink/?LinkId=234238
Imports System.Reflection
''' <summary>
''' Une page vide peut être utilisée seule ou constituer une page de destination au sein d'un frame.
''' </summary>
Public NotInheritable Class Acceuil
    Inherits Page
    ' Dim f3 As New hamza
    ' Dim start_frm As New start_form
    Dim f2 As New Operations

    Private Sub Confgrt(sender As Object, e As RoutedEventArgs) Handles Config.Click
        Frame.Navigate(GetType(Configs))
    End Sub

    Private Sub load(sender As Object, e As RoutedEventArgs)
        Dim buffer As String
        Dim champ As String
        Dim valeur As String
        '------------------------ lire fichier be_mobile.ini -------------------'
        Try
            Dim FILE_NAME As String = "C:\Users\baseInfo\Desktop\MagasinComplet\MagasinComplet\bin\x86\Debug\AppX\Multi_magasin.txt"
            Dim objReader As New System.IO.StreamReader(FILE_NAME)

            Do While objReader.Peek() <> -1
                buffer = objReader.ReadLine()
                If String.IsNullOrEmpty(buffer) Then Continue Do
                champ = get_champ(buffer)
                valeur = get_valeur(buffer)
                Select Case champ
                    Case "Mg1" : mg1.Content = valeur : mg1.Visibility = True
                    Case "Mg2" : mg2.Content = valeur : mg2.Visibility = True
                    Case "Mg3" : mg3.Content = valeur : mg3.Visibility = True
                    Case "Mg4" : mg4.Content = valeur : mg4.Visibility = True
                    Case "Mg5" : mg5.Content = valeur : mg5.Visibility = True
                End Select
            Loop

            '/ Dim m As MvtEntree
            '/   Application.Start(App2.MvtEntree.)
            '/objReader.Close()
        Catch ex As Exception
            MsgBox("fichier ini de multi_mag est introuvable : " & ex.Message)
        End Try
    End Sub

    Private Sub Mg1Click(sender As Object, e As RoutedEventArgs) Handles mg1.Click
        '    chemin_ini = "\magasin1.txt"
        Frame.Navigate(GetType(Authentification))
        'Select Case Interfaces
        'Case 1 : load_start_frm() : If test_fichiers_NE() <> 0 Then Frame.Navigate(GetType(recuperer)) Else start_form.Show()
        '    Case 2 : load_form_0() : If test_fichiers_NE() <> 0 Then Frame.Navigate(GetType(recuperer)) Else Frame.Navigate(GetType(Operation))
        'Case 3 : y = 34 : visible_import = False : If test_fichiers_NE() <> 0 Then Frame.Navigate(GetType(recuperer)) Else hamza.Show()
        'End Select
        '   Me.Hide()
    End Sub

    Private Sub Mg2Click(sender As Object, e As RoutedEventArgs) Handles mg2.Click
        'chemin_ini = "\magasin2.txt"
        Frame.Navigate(GetType(Authentification))
        'Select Case Interfaces
        'Case 1 : load_start_frm() : If test_fichiers_NE() <> 0 Then Frame.Navigate(GetType(recuperer)) Else start_form.Show()
        'Case 2 : load_form_0() : If test_fichiers_NE() <> 0 Then Frame.Navigate(GetType(recuperer)) Else Frame.Navigate(GetType(Operation))
        'Case 3 : y = 34 : visible_import = False : If test_fichiers_NE() <> 0 Then Frame.Navigate(GetType(recuperer)) Else hamza.Show()
        'End Select

        'Me.Hide()
    End Sub

    Private Sub Mg3Click(sender As Object, e As RoutedEventArgs) Handles mg3.Click
        'chemin_ini = "\magasin3.txt"
        Frame.Navigate(GetType(Authentification))
        'Select Case Interfaces
        'Case 1 : load_start_frm() : If test_fichiers_NE() <> 0 Then Frame.Navigate(GetType(recuperer)) Else start_form.Show()
        ' Case 2 : load_form_0() : If test_fichiers_NE() <> 0 Then Frame.Navigate(GetType(recuperer)) Else Frame.Navigate(GetType(Operation))
        'Case 3 : y = 34 : visible_import = False : If test_fichiers_NE() <> 0 Then Frame.Navigate(GetType(recuperer)) Else hamza.Show()
        'End Select

        'Me.Hide()
    End Sub

    Private Sub Mg4Click(sender As Object, e As RoutedEventArgs) Handles mg4.Click
        'chemin_ini = "\magasin4.txt"
        Frame.Navigate(GetType(Authentification))
        'Select Case Interfaces
        'Case 1 : load_start_frm() : If test_fichiers_NE() <> 0 Then Frame.Navigate(GetType(recuperer)) Else start_form.Show()
        '     Case 2 : load_form_0() : If test_fichiers_NE() <> 0 Then Frame.Navigate(GetType(recuperer)) Else Frame.Navigate(GetType(Operation))
        'Case 3 : y = 34 : visible_import = False : If test_fichiers_NE() <> 0 Then Frame.Navigate(GetType(recuperer)) Else hamza.Show()
        'End Select

        'Me.Hide()
    End Sub

    Private Sub Mg5Click(sender As Object, e As RoutedEventArgs) Handles mg5.Click
        'chemin_ini = "\magasin5.txt"
        Frame.Navigate(GetType(Authentification))
        'Select Case Interfaces
        ' Case 1 : load_start_frm() : If test_fichiers_NE() <> 0 Then Frame.Navigate(GetType(recuperer)) Else start_form.Show()
        '    Case 2 : load_form_0() : If test_fichiers_NE() <> 0 Then Frame.Navigate(GetType(recuperer)) Else Frame.Navigate(GetType(Operation))
        '  Case 3 : y = 34 : visible_import = False : If test_fichiers_NE() <> 0 Then Frame.Navigate(GetType(recuperer)) Else hamza.Show()
        'End Select

        'Me.Hide()
    End Sub

    Private Sub fermer(sender As Object, e As RoutedEventArgs)
        Application.Current.Exit()
    End Sub
End Class
